#!/usr/bin/env bash
export SPARK_MASTER_IP=`hostname --ip-address`
export SPARK_LOCAL_IP=`hostname --ip-address`
/usr/local/spark/sbin/start-master.sh --properties-file /spark-defaults.conf -i $SPARK_LOCAL_IP "$@"
/bin/bash
